## Privacy Impact Assessment

_This assessment meets a compliance requirement under data privacy laws when GitLab processes personal data as a Data Controller, in connection with certain high-risk processing activities._   

### Step 1 - Name Issue

- [ ] Name the issue as follows: PIA for High-Risk Processing Activity - [Processing Activity]

# Privacy Impact Assessment

## Overview
**Project Name**: [Project Name]  
**Assessment Date**: [Date]  
**Assessment Owner**: [Name]  
**Review Due Date**: [Date]

## Trigger Category
Select all that apply:
- [ ] Targeted Advertising: is advertising displayed to a consumer that is selected based on personal data obtained from that consumer's activities over time and across nonaffiliated websites or online applications to predict the consumer's preferences or interests. 
- [ ] Sale of Personal Data
- [ ] Processing of Sensitive Data
- [ ] Profiling with Risk of Harm
- [ ] Other High-Risk Processing (specify)

## Processing Activity Details
### Description
Provide a detailed description of the processing activity, including:
- What personal data will be processed
- Purpose of processing
- How data will be collected
- How data will be used
- Data retention period
- Data sharing with third parties (including all vendors and third-parties that facilitate our targeted advertising activities):

### Data Categories
List all categories of personal data involved:
- [ ] Basic Personal Information _(such as name, username/user ID)_
- [ ] Contact Information (_such as email, phone, address)_
- [ ] Sensitive Data _(such as age, gender, ethnicity) (please specify what sensitive data is processed)_
- [ ] Biometric Data _(such as voice/video recordings)_
- [ ] Location Data _(such as IP address, country/region/city/postal code)_
- [ ] Internet/Network Activity _(such as advertising ID, GA ID, device/browser, browser fingerprint, session replay data)_
- [ ] Other (specify)

## Benefit Analysis
### Direct Benefits
- To Controller:
  - [List benefits]
- To Consumer:
  - [List benefits]


## Risk Analysis

### Risk Categories to Consider
- [ ] Unfair/Deceptive Treatment
- [ ] Unlawful Discrimination
- [ ] Financial Harm
- [ ] Physical Harm
- [ ] Reputational Harm
- [ ] Privacy Intrusion
- [ ] Loss of Autonomy
- [ ] Other Substantial Injury

### Identified risks
For each risk category identified, specify:
- Description of risk as it applies to data subjects
- Likelihood (Low/Medium/High)
- Impact (Low/Medium/High)
- Affected party(ies)

## Safeguards and Controls
### Existing Safeguards
[List current safeguards]

### Mitigation Effectiveness
[Assess how effectively the safeguards mitigate each identified risk]

### Privacy Principles
Assess alignment with privacy principles:
- Data Minimization
- Purpose Limitation
- Storage Limitation
- Data Security
- Transparency

## Contextual Assessment
### Consumer Expectations
- [ ] Aligned with reasonable consumer expectations
- [ ] May exceed consumer expectations (explain)
- [ ] May conflict with consumer expectations (explain)

### Controller-Consumer Relationship
[Describe nature of relationship and any relevant factors]

### Deidentification Assessment
- [ ] Can data be deidentified?
- [ ] Impact of deidentification on processing purpose
- [ ] Deidentification methods considered

## Compliance Assessment
### Legal Requirements
[List applicable legal requirements and how they are met]

## Recommendation
- [ ] Proceed as Planned
- [ ] Proceed with Additional Safeguards
- [ ] Substantial Modifications Required
- [ ] Do Not Proceed

### Justification
[Provide reasoning for recommendation]

## Required Approvals
- [ ] Privacy Team
- [ ] Legal Team
- [ ] Security Team
- [ ] Business Owner
- [ ] Other (specify)

## Documentation
### Supporting Documents
[List all relevant documentation and link]

### Retention
This assessment must be retained for a minimum of 5 years.

## Review Schedule
- Next Review Date: [Date]
- Triggering Events for Earlier Review:
  - Major changes to processing activity
  - New risks identified
  - Changes in applicable law
  - Incidents or breaches

/label ~privacy ~compliance ~data-protection
