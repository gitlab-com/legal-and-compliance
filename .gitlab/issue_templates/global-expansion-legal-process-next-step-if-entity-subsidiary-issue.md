# 'Next step' issue template for global expansion legal process when the recommended solution is an _entity/subsidiary_

_This template should be used to manage the 'next step' in the global expansion legal process of gathering, processing and assessing information when it's recommended to open or expand in a country for hire **using an entity/subsidiary**_

This should be used _after_ an initial issue has been opened to assess the current status of the country, gather and review preliminary information and once a decision has been made to recommend an entity/subsidiary solution.

The full legal process/playbook for global expansion is set out [here](https://docs.google.com/document/d/1pw5ji3IuAgUiMIJLNPIOU_ADNHtYrAN0iOKXxJjlcAk/edit).

_Note that there are issue templates for additional/further solutions if needed:_
- _PEO/EOR issue template [here](https://gitlab.com/gitlab-com/legal-and-compliance/-/blob/master/.gitlab/issue_templates/global-expansion-legal-process-next-step-if-PEO-EOR-issue.md)_
- _Branch issue template [here](https://gitlab.com/gitlab-com/legal-and-compliance/-/blob/master/.gitlab/issue_templates/global-expansion-legal-process-next-step-if-branch-issue.md)_

During the initial review, a folder will have been created for the relevant country in the shared [Global expansion drive](https://drive.google.com/drive/folders/0ALzUKh4XsBAsUk9PVA?usp_dm=true) and an initial issue opened. These should be linked in this issue and files and correspondence saved to the folder in the global expansion drive for transparency and good matter management. _You can tag Sarah Rogers and request assistance with filing materials if needed._

## **Issue Creation Steps**

* [ ] 1. Appropriately title the issue according to the country being assessed using this format: Global expansion - legal review process - entity/subsidiary issue - COUNTRY NAME

* [ ] 2. Please mark this issue as confidential and assign to:
    * [ ] - Emily Plotkin and Sarah Rogers
    * [ ] - Darren Burr (EMEA) _OR_ Tara Kumpf (APAC) (depending on the location of the country)
    * [ ] - Rashmi Chachra
    * [ ] - Lynsey Sayers
    * [ ] - Matthew Taylor
    * [ ] - Harley Devlin
    * [ ] - Someone in payroll? _(decide with Emily)_
    * [ ] - Someone in total rewards? _(decide with Emily)_
    * [ ] - Someone in tax? _(decide with Emily)_
    * [ ] - Someone in finance? _(decide with Emily)_

* [ ] 3. Add appropriate labels (global expansion legal process labels are in green)

* [ ] 4. Add a link to the initial review issue for [country] [here] and to the country folder in the global expansion drive [here].

* [ ] 5. Add a due date _(discuss with Emily whether this is appropriate or necessary on a case-by-case basis)_

## Legal process steps for Entity/Subsidiary:

_i. If Entity/Subsidiary then for **Employment**_

1. What employment terms and conditions must be included in the contract?

    a. Probationary periods

    b. Applicable collective bargaining agreements

    c. Work rules

    d. Required terms

    e. Time tracking and/or overtime and what processes need to be in place

    - If there is overtime, can it be included in the base salary offered?

    f. Information on notice periods at end of employment

    g. Process for dismissals

    - Severance payments

    - Redundancy process and/or payments

2. What payroll providers exist and what are the downstream implications on our payroll team?
3. What services will the payroll providers provide?
4. Total Rewards

    a. Are there pension or other statutory benefit requirements?

    b. What additional benefits can the company provide?

    c. How does that compare to what the team members currently have and what is required in market? 

    d. If overtime must be included can we include it in the base pay in the first place?


5. What additional policies do we need or supplements to our global policies?
6. Additional compliance - headcount threshold requirements? works council or personnel representatives required? when?
7. Background checks
8. Privacy
9. If we have current team members in the location, what do we need to do to convert them to branch employees?
10. Sales compensation plans
11. Benefits
12. Tax withholding (income tax, social costs, entitlements)
13. Pay raise mandates from the government?  (e.g., Brazil, Colombia, Greece, Malaysia, Turkey)
14. Extra pay (13-month installment)

    a. one time payment

    b. Holiday bonus

    c. Spread across the year


15. Gender equity and reporting
16. How long is it going to take to establish? (average 6-9 months)
17. Equity issues - is equity permitted/supported in this country , what are the implications for our team members, any for GitLab? 

_ii. If Entity/Subsidiary then for **Corporate**_

1. Coordinate with Tax and Finance on appropriate entity formation

2. Coordinate with Employment team on Equity setup

_iii. If Entity/Subsidiary then for **Product and Privacy**_
1. Review employment privacy requirements for direct employees of a subsidiary

2. Review any import/export requirements regarding that the company will have an entity in this location

_iv. If Entity/Subsidiary then for **Commercial**_
1. Review commercial implications of having an entity in this location

2. Any commercial implications or effect on FedRamp for team members in location?

### Steps once approved
1. * [ ] Input into business justification doc if needed
1. * [ ] Inform EPLI carrier of move to new location
2. * [ ] Determine DRI for setting up entity/subsidiary
3. * [ ] Support Employment Solutions Specialist with informing team members and opening issue for conversion to new employment solution
4. * [ ] Review employment contracts
5. * [ ] Determine if commission plan needs translation or any specific provisions for particular location
6. * [ ] Review applicable employment policies
7. * [ ] Work with Privacy team for any specific updates


## Final steps in this issue
* [ ] Remove the label 'Global expansion legal process: underway' (which will automatically apply the 'Global expansion legal process: complete' label) and then close the issue.
* [ ] Check to ensure the related initial issue for the legal process for [country] has been updated, with relevant links added, the label 'Global expansion legal process: underway' removed (which will automatically apply the 'Global expansion legal process: complete' label) and the issue closed, if needs be.  

/confidential