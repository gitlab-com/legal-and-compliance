#Competitive Benchmarking - Procurement Request  

<!--This template should be used by team members who want to procure software for the purpose of competitive benchmarking. To enable Legal & Corporate Affairs to review your request efficiently, provide all of the requested information below.

REMINDER: Seek approval for the purchase via this issue BEFORE opening a Zip Request. Legal should respond within 2 business days. If approved, you will still need to follow the standard procurement process outlined here: https://about.gitlab.com/handbook/finance/procurement/.  

-->

* Provide the link to the terms of the competitive software you would like to procure:


* Explain in detail the proposed use case and business justification for the procurement request. 


* Is there any particular urgency associated with the request that the Legal & Corporate Affairs team should be aware of?


### Do Not Edit Below
/confidential
/label ~"legal-procurement::to do" ~"New" ~"Product & IP"
/assign @dcolesjr
/assign @igorman