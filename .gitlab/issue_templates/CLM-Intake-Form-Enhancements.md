## CLM Intake Form Enhancements

### Directions
Use this template to request changes to the DocuSign CLM Intake Form. Please submit a separate issue for each new field or update. Use the **exact wording** you would like to see on the Intake Form for the field name, help text, and field values (only ( ) and ? can be used as special characters). Once submitted, the change must be approved by Commercial Legal leadership before Legal Ops can update the form.

Date Needed:

### Proposed Change
Field Name:

Help Text:

Field Type:
- [ ] Attachment
- [ ] Checkbox
- [ ] Currency
- [ ] Date
- [ ] Dropdown
- [ ] Number
- [ ] Radio Buttons
- [ ] Text Field

Field Values:

Default Value:

Required Field:
- [ ] Yes
- [ ] No

Please describe field logic or anything else that would be helpful to know (location on form, etc.):

### Approver
- [ ] @m_taylor

/assign @ktesh

/confidential
