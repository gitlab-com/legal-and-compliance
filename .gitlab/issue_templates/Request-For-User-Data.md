## When to use this template

This template is used to request queries for user data related to Event Data, Website Usage Data and Device Information pursuant to either a valid law enforcement request or a data subject access request. Generally, this template will be created by a member of the Privacy Team, but may be used by other Team Members.

**All requests for User Data must be approved by GitLab Legal. The approver must add the ~"legal approved" labe to the issue.**

### What is the reason for the request for user data?
 
   - [ ] Valid law enforcement request. GitLab Internal Reference No:  <- list the internal reference no. from the tracking sheet>
   - [ ] Data subject access request.  Original request issue: <- link to the original request here>
                                       Meta issue:  <- link to the meta issue here>

### Types of User Data required

- [ ] Event Data. This should include service usage data plus event analytics such as browsing duration, page clicks, page views (this is fully described in our [Privacy Statement](https://about.gitlab.com/privacy/#information-about-your-use-of-the-services-we-collect-automatically))

- [ ] Website Usage Data. This should include referral information, date/time of visit, pages, viewed, links clicked, etc (this is fully described in our [Privacy Statement](https://about.gitlab.com/privacy/#information-about-your-use-of-the-services-we-collect-automatically)). Website Usage Data would also include device information and identifiers such as device type, OS, browser type and version, language preference, IP address, etc)

- [ ] Cookie Data Associated with a User. 

### Identifiers for use in queries

Please provide the following identifiers to be used to perform queries:

**Namespace ID**: ---------

**User ID** --------

**Last known sign-in IP**: -------

**Remote IP**: -------  <- can be pulled from Rails prod logs>

### Time Period

Please specific the time period to query

- [ ] 30 days beginning on `/date`
- [ ] 90 days beginning on `/date`
- [ ] 365 days* beginning on `/date`
- [ ] All dates 
- [ ] Custom date range*:  <--enter the custom date range>
      _*Please note that if query terms do not allow a custom date range or specific date range, the query will be run for `all dates` and Privacy will need to redact any date ranges which are not relevant to the request._

**Due Date**: 
`/due` <- set the due date here if the request is time sensitive>

### Data Collection

_MS&A Tasks_

1. [ ]  Determine if Snowflake contains the Namespace ID data using the following query:

        SELECT *
        FROM PROD.COMMON.DIM_NAMESPACE
        WHERE dim_namespace_id = 'xxxxxxx';

        SELECT *
        FROM PROD.COMMON.FCT_EVENT 
        WHERE dim_ultimate_parent_namespace_id = 'xxxxxxx';

        SELECT *
        FROM PROD.COMMON.FCT_BEHAVIOR_STRUCTURED_EVENT
        WHERE dim_namespace_id = 'xxxxxxx';

    - [ ] Save query results to the Google Drive Folder: <-- Privacy to link to folder>
    - [ ] Namespace ID is NOT available.

1. [ ]  Run the following queries to connect Namespace ID from Snowplow to Google Analtyics, adjusting the `PROD.LEGACY.SNOWPLOW_PAGE_VIEWS_`  and `visit_start_time` for the time period noted above:

        select distinct GAS.*
        from PROD.LEGACY.SNOWPLOW_PAGE_VIEWS_all as SP
        right join PROD.LEGACY.GA360_SESSION as GAS on SP.gsc_google_analytics_client_id = GAS.CLIENT_ID
        where gsc_namespace_id = 'xxxxxxx'
        and visit_start_time > 'date' 
        order by visit_start_time

        select distinct GAH.*
        from PROD.LEGACY.SNOWPLOW_PAGE_VIEWS_all as SP
        right join PROD.LEGACY.GA360_SESSION as GAS on SP.gsc_google_analytics_client_id = GAS.CLIENT_ID
        right join PROD.LEGACY.GA360_SESSION_HIT as GAH on GAS.VISIT_ID = GAH.VISIT_ID and GAS.visitor_id = GAH.VISITOR_ID
        where gsc_namespace_id = 'xxxxxxx'
        and GAH.visit_start_time > 'date'
        order by GAH.visit_start_time

    - [ ] Save query results in a Google Sheet to the Google Drive Folder linked above.
    - [ ] No Website Usage Data was returned. Please comment if there is a likely reason for no returned results:  

1. [ ] Tag Privacy in a comment for review.

_Privacy Tasks_

1. [ ] Review saved query results against data requested in the law enforcement request.

1. [ ] Perform all necessary redactions for date range exceptions

1. [ ] Perform all necessary redactions for data fields unresponsive to the law enforcement request, including any proprietary fields or data not in scope of the legal process served.

1. [ ] Close issue when production has been served on law enforcement agency. 


/label ~Privacy::Intake

/label ~MS&A::Data-Ops

/label ~MktgSandA:0-Triage

/label ~confidential

/assign @dennischarukulvanich @christinelee

/assign @BronwynBarnett

/label ~"keep confidential"

/due in 1 week

/cc @lasayers

/confidential



