## Third Party Risk Assessment

_This issue is meant to highlight Privacy risk identified by a [Vendor Privacy Review Process](https://about.gitlab.com/handbook/legal/privacy/#vendor-privacy-review-process) to the third party's business owner. The business owner may either accept/take the risk or discontinue sharing/storing/transmitting [Orange/Red](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) GitLab Personal Data with/to the third party._

## Privacy Team Member to Complete

### Step 1 - Name Issue

- [ ] Name the issue as follows: Vendor/Software Name - Privacy Vendor Risk Acceptance - Risk Owner Name

### Step 2 - Link Issues/Reqs/Department

- [ ] Procurement issue or Coupa requisition
- [ ] Vendor Privacy issue, if applicable
- [ ] Add label to issue indicating the Risk Owners Department

### Step 3 - Add General Information Related to Risk

- [ ] Complete table below.

|  | To be completed by GitLab Privacy team |
| ------ | ------ |
| Third party name | `Enter the legal name of the third party here` |
| System/Application name (if applicable) | `If the third party is providing an application (free or paid), enter it here. Otherwise enter "N/A"` |
| Third party contact information | `Please provide the name and email address of a security representative for this third party` |
| Which [data elements](https://internal-handbook.gitlab.io/handbook/security/data_classification/) will this third party have access to? | `Please specify the data this third party will have access to` |
| [Data classification](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) | `Enter the classification of data that is accessible by the Third Party here` |

### Step 4 - Document Risk Statement and Opinion

- [ ] Risk statement and opinion added.

`Privacy to enter risk statement here. For example: Vendor intends to include cookies on our site but will not enter into a Service Provider agreement with GitLab.  Further, Vendor does not have update to data Standard Contractual Clauses.  Privacy cannot recommend the use of this third party.`

### Step 5 - Update Approvals section based on risk rating.

- [ ] Only the Approvals required for this risk acceptance remain in the Apply Approval section updated. For example, if this is for a Moderate Risk, the Low Risk Approval and High Risk Approval blocks should be deleted and only the Moderate Risk approval block should remain.

### Step 6 - Make issue confidential

- [ ] Issue made confidential.

### Step 7 - Quality Review (Optional)

'If desired, receive a review of the risk justification and risk rating by another Privacy team member.'

- [ ] Risk justification is sufficient
- [ ] Risk rankings aligns to vendor's inherent risk
- [ ] Quality Review complete

'Add Privacy reviewer's name, title and date'

## Risk Owner and Additional Approvers to Complete

### Provide justification for taking this risk

- [ ] Risk justification provided

`Risk Owner to document rationale for taking this risk.`

### Apply Approval

The approvals below are necessary to accept/take this risk.

Low Risk Approval
- [ ] `tag contact` - Risk Owner
- [ ] `tag contact` - Manager (If the Risk Owner is a Manager, no additional Manager level approval is required)

Moderate Risk Approval
- [ ] `tag contact` - Risk Owner
- [ ] `tag contact` - VP (If the Risk Owner is a VP, no additional VP level approval is required)
/label ~"TPRM::Moderate Risk"

High Risk Approval
- [ ] `tag contact` - Risk Owner
- [ ] `tag contact` - Director/VP 
- [ ] `tag contact` - E-Group

/confidential
/label ~"Privacy Vend Rev::Intake"
/label ~"Privacy"
/label ~"Privacy Vend Rev::Business Owner To-Do"
