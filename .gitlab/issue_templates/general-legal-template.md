<!--This template is for general questions related to deliverables and non-sensitive information. Please be aware that all information provided in this Issue Template is visible to all GitLab team members. If you have a sensitive, private, or confidential request, or are otherwise seeking legal advice, DO NOT use this template. Instead, please send an e-mail to legal@gitlab.com

Some examples, which should be sent to legal_internal@gitlab.com include (but are not limited to): HR matters, requesting or seeking legal advice, and any matters relating to litigation or potential litigation. See further information regarding privileged information in the legal [handbook](https://about.gitlab.com/handbook/legal/#1-the-attorney-client-relationship-in-the-united-states)-->

### Requestor Details:
<!-- In the case of Legal determining that this question needs to be handled in a more confidential manner, discussion may move to email or Slack. -->
* Slack handle: `Slack handle here` 
* Email address: `Email address here`

---
---
### What category applies to your request?
- [ ] Applied the correct category label to the issue side bar
<!-- Choose the appropriate labels from the list below and apply it to the issue. Do not simply note the label within the issue description.--> 

* `Contracts`
* `Employment`
* `IP` 
* `Legal Compliance` 
* `Licensing`
* `Privacy`
* `Procurement` (Use `legal-procurement::to do` label)
* `RMDR` (For Risk Management/Dispute Resolution requests, please complete the RMDR Intake Form issue template)
* `Triage` (Use “Triage” for other categories or if you’re not sure which label is appropriate)

### Is this time sensitive?
<!-- Add a due date to the issue side bar, if you have a deadline you are working under. -->

### What is the desired deliverable/outcome?
<!-- Write a 1-sentence or less summary of what needs to happen with this issue. Ex: “I need to have a questionnaire filled out.” "I need approval for a marketing promotion.” -->

### What are the details of your request?
<!-- Give more thorough write up of what your request is. Provide links to other issues, Slacks, etc. in your description. Attach any related documents, as well. -->

---
### Do Not Edit Below

/confidential

/label  ~"New"
