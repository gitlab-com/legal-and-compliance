## Tech Stack AI Features Review

_This Legal assessment must be peformed any time:_ 

1. _an existing Tech Stack system deploys AI features that are accessible under our current subscription and the Business/Technical Owner_:

- _Would like obtain approval for the use of AI features in the existing Tech Stack system; or_
- _Cannot prevent use of the AI features by other GitLab users because the features cannot be disabled in the Admin console_

_New Vendors, including any attendant AI features, will be evaluated in Zip as part of standard Procurement review._

2. _a team member requests access to a Claude API key_

### Select request type, name the issue appropriately, and populate the relevant section:

- [ ]  **Tech Stack AI Features Review**
    - Populate _Section A_ below, and name the issue as follows:

`    System - Tech Stack Business or Technical Owner - TS AI Features Review`

- [ ] **Claude API Key Request** 
    - Populate _Section B_, and name the issue as follows:

`Claude API Key - [Team Member Name] - TS AI API Key Access Review`

## Requestor to Complete

### Section A - Tech Stack AI Features Review

#### Step 1 - Provide Context and Attach Relevant Documentation: 

- [ ]  Briefly describe the ways in which GitLab uses the Tech Stack system and the kinds of GitLab data this system has access to: `Please enter your answer here.`  
- [ ]  Link/attach documents to the terms that would apply to these AI features. <!-- Vendors often release additional terms that would apply to their AI features - please attach any that would apply. If there are no new terms related to recently deployed AI features, please link/attach our existing agreement(s) with the vendor (e.g. the MSA, Data Processing Agreement, etc.) -->
- [ ]  Link/attach documents to other AI documentation on vendor's website. 


#### Step 2 - Please explain if AI features for this system can be turned off or gated behind Admin controls.  If they cannot, please state as much:

- [ ] Feature(s) can be turned off by Admin Controls.
- [ ] Feature(s) cannot turned off by Admin Controls.
    - [ ] Please provide the reasons why the Feature(s) cannot be turned off. 


#### Step 3 - Please explain how individual users are made aware of the processing of data by AI features and if they would reasonably expect such processing to occur (e.g., systems owner informed other GitLab users; GitLab users received policy updates from the Vendor directly):

`Please enter your answer here.`


#### Step 4 - Please state how long inputs/outputs from AI features are retained by the Vendor.  If there is an option for zero-day retention, please state that this is an option.

- [ ] Duration of inputs/outputs from AI Features retained by the vendor.<!--Select one-->
    - `Enter Number of Days/Weeks/Months/Years`; or
    - `Indefinite Retention`
- [ ] Zero-day retention option available.


#### Step 5 - Please explain where Output from AI features will live, i.e., where will the output be stored or embedded into existing GitLab systems.

`Please enter your answer here.`
 

#### Step 6 - Please explain whether the system will process any usage data related to AI features and whether that data will be de-identified in any way.

- [ ] Yes, usage data is collected.
  - Will the data be de-identified in any way? If yes, describe how data will be de-identified. `Please enter your answer here.`

- [ ] No.


#### Step 7 

- [ ] Please open a [TPRM Issue template](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-risk-team/third-party-vendor-security-management/-/issues/new) for separate completion since AI features processing data may be a material change not previously covered by Security's prior approval.
- [ ] Link TPRM Issue here.


## Legal Privacy and IP to Complete

| Risk | Remediation|
| ------ | ------ |
| describe risk | describe remediation |
| describe risk | describe remediation | 

**Add to Personal Data Request template?**

  - [ ] Yes
  - [ ] No

---

## Section B: Claude API Key Request

### Requestor to Complete

#### Step 1 - Project Information
Briefly describe what you intend to develop using the Claude API key:

#### Step 2 - Data Classification Usage

- What data types do you intend to use for this development:
    - Describe data types: Enter description
    - Confirm [data classification](https://handbook.gitlab.com/handbook/security/data-classification-standard/) level: Enter classification
    - [ ] Confirm that [RED data](https://handbook.gitlab.com/handbook/security/data-classification-standard/#red) will not be used for this purpose

## Approval

- [ ] Approved
- [ ] Approved upon implementation of risk remediation
- [ ] Not Approved
- [ ] Needs further consideration

<!--Do not edit below this line-->

/confidential

/label ~legal::AI TS review 
