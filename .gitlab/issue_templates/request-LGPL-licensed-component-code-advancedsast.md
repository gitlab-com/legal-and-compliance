## When to use this template

This template is used to document requests for LGPL-licensed component source code from Ultimate users. These requests will likely come in the form of an email to legal@gitlab.com and/or through a support ticket.

All requests for LGPL-license component code related to Advanced SAST must be reviewed and approved by GitLab Legal. 

### Confirm request is coming from a User with an active Ultimate licence subscription. 

[Yes/No]

### What version of Advanced SAST does the request relate to? 

**Note: A user can confirm the version, which differs from the GitLab version, by referencing the info top line included in the CI job log. For example: GitLab Advanced SAST analyzer v.1.0.30**

/confidential

/label ~Product & IP

/cc @igorman 
