#Product Development - Legal Risk Checklist Requests

<!--This template should be used by team members to discuss potential legal risks identified during the product development workflow. To enable Legal & Corporate Affairs to review your request efficiently, provide all of the requested information below.

REMINDER: Development can continue during this review, however, the planned development must not be released prior to legal input being received and actioned, and subject to the understanding that changes may be required to mitigate identified risk. Legal respond within 2 business days. 

-->

* Provide the link to the related Product or Engineering Issue:


* Copy the [Legal Review Checklist](https://internal-handbook.gitlab.io/handbook/legal-and-compliance/legal-risk-checklist) question number to which your concern relates (if it relates to an Artificial Intelligence question, write “Artificial Intelligence”):


* Provide further details on the concern:
 

* Is there any particular urgency associated with the planned development that the Legal & Corporate Affairs team should be aware of?



### Do Not Edit Below
/confidential
/label ~"LRC" ~"New"
