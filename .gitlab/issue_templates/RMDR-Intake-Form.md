### Requestor Details:
<!-- In case RMDR determines that this question needs to be handled in a more confidential manner, discussion may move to email or Slack. -->
* Slack handle: `Slack handle here` 
* Email address: `Email address here`

### Check the box to confirm this issue does not involve parties external to GitLab:
- [ ] I confirm that this request does **not** involve parties external to GitLab. If this issue involves any external parties, email the details of your request to legal_internal@gitlab.com.

---
---

### Is this time sensitive?
<!-- Add a due date to the issue side bar, if you have a deadline you are working under. Unless circumstances dictate otherwise, requesters can generally expect a 2-3 business day turnaround. -->

### What are the details of your request? Briefly explain what advice you are seeking.
<!-- Please include enough details to give the full context of your request. -->

### Complete the following steps.
- [ ] Attach any documents that may affect our analysis of this issue. If such documents exist, but you don't have them, please advise where we might be able to find them:

- [ ] Let us know who you have spoken with about this issue, and any details of the conversation or correspondence:

- [ ] List any other individuals with whom we should discuss this issue to have a full understanding:

---
### Do Not Edit Below

/confidential
/assign @dhodes
