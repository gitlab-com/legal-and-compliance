<!--When to use this template: This template is for contract review that **_DOES NOT_** fall into procurement or sales. Please read the Legal Handbook [instructions for engaging Legal](https://about.gitlab.com/handbook/legal/#how-to-partner-with-the-gitlab-legal-team-to-get-what-you-need) to ensure that you are using the correct avenue to facilitate a timely and efficient response to your requests.-->

### Sales and Procurement Requests **DO NOT USE THIS TEMPLATE**
**SALES REQUESTS**: For all legal requests related to revenue, or potential revenue activity, please follow the [Legal Request Process](https://about.gitlab.com/handbook/legal/customer-negotiations/#how-to-reach-legal)

**PROCUREMENT REQUESTS**: For all legal requests related to purchasing, or potential GitLab purchasing / spending money, please follow the [Procurement Process](https://about.gitlab.com/handbook/finance/procurement/#-procurement-toolkit)

 
### Requestor Details:
<!-- In the event Legal determines requestion and/or question needs to be handled in a more confidential manner, discussion may move to email or Slack. -->
* Slack handle: `Slack handle here`
* Email address: `Email address here`
 
---
---
### What type of Agreement or Contract is being requested for review?
- [ ] NDA
- [ ] Partner Agreement
- [ ] Media Release
- [ ] Other - please specify:
 
### Is this time sensitive?
<!-- Add a due date to the issue side bar, if you have a deadline you are working under. If the deadline is in less than 48 hours, link to this issue in the #Legal Slack channel.-->

### Provide context
<!--Provide information about why this contract review is necessary.-->

### Link to document for review
<!--Upload the document for review in this section.-->
 
### Link to approved document
<!--The Contracts team will upload the approved, stamped document in this section when it is ready.-->
 
---
### Do Not Edit Below
 
/confidential
 
/label  ~"New" ~”Contracts” ~"Contract Review::Intake"
