### Gong Recording Legal Approval

_This issue should be completed for Legal aproval to retain a Gong recording beyond the established 1 year retention period._

_A request to retain a recording must be received by Legal no later than 15-days prior to the end of the 1 year retention window. For example, a recording that was created on May 1 and will automatically be deleted 1 year later on April 30; therefore a request must be received by Legal no later than April 15._ ***Please note that any request submitted within 14-days of the end of the retention window may not be reviewed in time or approved.***

### For additional information related to Gong recordings, please consult the:

- [Gong Security & Privacy Controls](https://internal.gitlab.com/handbook/sales/sales-tools/gong/)

### Approval Guidelines

- Any recording approved for retention beyond 1 year must not be shared externally.

- Any recording approved for retention beyond 1 year will be moved to a Legally Approved folder in Gong and/or a copy of the approved recording or snippet will be downloaded to a shared Drive folder.  

- The recording cannot be kept indefinetly and the requester will be responsible for deleting approved recordings retained outside the 1 year window. 

- There must be a compelling business reason to keep a full recording beyond 1 year. Recordings retained for training purposes will become stale over time and should be replaced with more recent examples.  Therefore, a full recording that is a good example of a certain practice is not a compelling business reason that will be approved, unless the requester can demonstrate that the example will not be easily repeatable and the example cannot be reduced to a snippet.
  
-----  

# Requestor to Complete

### Step 1 - Name Issue
 
 Name the issue as follows: Gong Recording [date of recording]- Approval - Requestor's Name 

### Step 2 - Link Recording

- [ ] Link to the snippet you want to keep: </details>

- [ ]  Link to the full length recorded call you want to keep:  </details>


### Step 3 - Recording Details

### For requests involving a snippet of a call

 - [ ] How long is the snippet? (provide in minutes:seconds format) 

 - [ ] What training will the snippet be added to in EdCast?  </details>


### For requests involving full recording of a call:

 - [ ] How long is the recording?  <minutes:seconds>


### Step 4 - Attestation:
 
- [ ] I affirm that this recording will not be shared with external parties or individuals outside of GitLab

- [ ] Provide a compelling business reason why the recording must be kept longer than 365-days:


### Step 5 - Validate SAFE framework requirements

- [ ] I have reviewed the [SAFE framework](https://about.gitlab.com/handbook/legal/safe-framework/) and affirm that the recording meets the requirements of the SAFE framework as follows:
- [ ] Sensivite Information, as defined in the SAFE framework, is not discussed in the recording
- [ ] All information discussed and disclosed is accurate and I am able to provide a reference and/or data to support the accuracy, including the DRI if I am not responsible for the accuracy of the information
- [ ] No forward looking statements are discussed and no financial elements are included in the recording and I can say unequivocally that no information is presented or discussed that would be concerning to GitLab's securities as a public company
- [ ] The effect of the information discussed about GitLab as a company has been reviewed and considered by me and my manager and no intentional or unintentional harm to the company or any team member could result 

-----

# Privacy Team to Complete


## Approval

- [ ] Yes, with new data retention period as defined below
- [ ] No. Recording will be deleted in accordance with the standard 1 year retention schedule

## Data Retention 

- [ ] This recording will be set to delete on: (insert date)


<!--Do not edit below this line-->
/confidential
/label ~"Privacy Vend Rev::Intake"
/cc: @kbetances 
