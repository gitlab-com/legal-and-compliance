# 'Next step' issue template for global expansion legal process when the recommended solution is _PEO/EOR_

_This template should be used to manage the 'next step' in the global expansion legal process of gathering, processing and assessing information when it's recommended to open or expand in a country for hire **using a PEO/EOR**_

This should be used _after_ an initial issue has been opened to assess the current status of the country, gather and review preliminary information and once a decision has been made to recommend a PEO/EOR solution.

The full legal process/playbook for global expansion is set out [here](https://docs.google.com/document/d/1pw5ji3IuAgUiMIJLNPIOU_ADNHtYrAN0iOKXxJjlcAk/edit).

_Note that there are issue templates for additional/further solutions if needed:_
- _Entity/subsidiary issue template [here](https://gitlab.com/gitlab-com/legal-and-compliance/-/blob/master/.gitlab/issue_templates/global-expansion-legal-process-next-step-if-entity-subsidiary-issue.md)_
- _Branch issue template [here](https://gitlab.com/gitlab-com/legal-and-compliance/-/blob/master/.gitlab/issue_templates/global-expansion-legal-process-next-step-if-branch-issue.md)_

During the initial review, a folder will have been created for the relevant country in the shared [Global expansion drive](https://drive.google.com/drive/folders/0ALzUKh4XsBAsUk9PVA?usp_dm=true) and an initial issue opened. These should be linked in this issue and files and correspondence saved to the folder in the global expansion drive for transparency and good matter management. _You can tag Sarah Rogers and request assistance with filing materials if needed._

## **Issue Creation Steps**

* [ ] 1. Appropriately title the issue according to the country being assessed using this format: Global expansion - legal review process - PEO/EOR issue - COUNTRY NAME

* [ ] 2. Please mark this issue as confidential and assign to:
    * [ ] - Emily Plotkin and Sarah Rogers
    * [ ] - Darren Burr (EMEA) _OR_ Tara Kumpf (APAC) (depending on the location of the country)
    * [ ] - Rashmi Chachra
    * [ ] - Lynsey Sayers
    * [ ] - Matthew Taylor
    * [ ] - Harley Devlin
    * [ ] - Someone in Talent Acquisition re roles to be hired in location and how they will be making offers _(decide who on a case-by-case?)_

* [ ] 3. Add appropriate labels (global expansion legal process labels are in green)

* [ ] 4. Add a link to the initial review issue for [country] [here] and to the country folder in the global expansion drive [here].

* [ ] 5. Add a due date _(discuss with Emily whether this is appropriate or necessary on a case-by-case basis)_

## Legal process steps for PEO/EOR

_i. If PEO/EOR, then for **Employment**_

1. What PEO can support this location?
2. What are the co-employment risks in this location?
3. What will the relationship be between the PEO and the team member?  

    a. Will it be serving as the Employer of Record where the team member is a direct employee of the PEO?

    b. Will there be a consulting relationship between the PEO and the team member?

    c. Temporary work agency model?

    d. Fixed term contract or indefinite term contracts?

4. What is covered in the services agreement between GitLab and the PEO?
5. Does the PEO have its own entity in country or is it working with a partner?
6. How does the PEO support equity?
7. How does the PEO support sales commission plans?
8. Can the PEO support all of GitLab’s roles (eg sales, engineering, G&A, marketing)

    a. Can a PEO support a Public Sector role?  Will we be desiring a Public Sector role in this location?

9. How does the PEO support GitLab’s IP?  Does it want to review our PIAA? 
10. What benefits does the PEO provide?  How do those relate to the benefits currently being offered team members in this location?
11. Are there different levels of employment in this location? Can the PEO support all levels? Are there any restrictions/implications with said levels? Benefit differences? ER statutory differences? Costs?
12. Contract Duration (Definite/indefinite) ie. Hungary has definite contracts with limitations (5 yrs max)
13. What services does the PEO provide?

    a. Employment Contract template creation?

    b. Background check services?

    c. Onboarding services?

    d. Human Resources services?

    e. Time tracking services?

    f. Policy creation? (including how these policies correlate with GitLab’s policies)

    g. Offboarding services?

    h. Costs per employee for use of the PEO (Are the services noted above included in the cost or extra?)

    i. Other services provided?  Costs of such services?

14. How long does it usually take to hire in this location?

    a. Are there particular months during the year that are difficult for hire?  (e.g., August if vacations)

    b. What type of notice period is typical to know when to develop pipeline
 
15. Is there a threshold number of team members in the location where the company should consider moving to a branch or entity?

16. Coordinate with Talent Acquisition on roles to be hired in location and how they will be making offers

_ii. If PEO then for **Corporate**_

1. Coordinate with Employment team on Equity setup

_iii. If PEO then for **Product and Privacy**_

1. Review employment privacy requirements for PEO team members

2. Review any import/export requirements regarding that the company will have team members in this location

_iv. If PEO then for **Commercial**_

1. Review commercial implications of team members in location (no team member in location should be signing contracts on behalf of company)

2. Any commercial implications or effect on FedRamp for team members in location?

3. Can PubSec be supported via PEO?

### Steps once approved
1. * [ ] Input into business justification doc if needed
1. * [ ] Inform EPLI carrier of move to new location
2. * [ ] Determine DRI for setting up PEO
3. * [ ] Support Employment Solutions Specialist with informing team members and opening issue for conversion to new employment solution
4. * [ ] Review employment contracts
5. * [ ] Determine if commission plan needs translation or any specific provisions for particular location
6. * [ ] Review applicable employment policies
7. * [ ] Work with Privacy team for any specific updates


## Final steps in this issue
* [ ] Remove the label 'Global expansion legal process: underway', which will automatically apply the 'Global expansion legal process: complete' label and close the issue.
* [ ] Check to ensure the related initial issue for the legal process for [country] has been updated, with relevant links added, the label 'Global expansion legal process: underway' removed (which will automatically apply the 'Global expansion legal process: complete' label) and the issue closed, if needs be.  

/confidential
