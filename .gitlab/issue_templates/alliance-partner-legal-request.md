**Requestor Details:**
- **_Slack handle:_** Slack handle here
- **_Email address:_** Email address here
- **_SFDC Case Link:_** Link here, if applicable 
- **_Other:_** Include other relevant links here (i.e., business plan, additional partner documentation, etc...)

**Type of Partner Agreement:**
- **Transactional (e.g. Payment and GTM terms):**
   - [ ] Technology Partner Agreement
   - [ ] Marketplace Partner Agreement
   - [ ] Cloud Partner Agreement 
   - [ ] OEM Partner Agreement
   - [ ] Other - please specify
- **Non-Transactional (e.g. Development w/o Payment or GTM):**
   - [ ] Technology Partner Agreement
   - [ ] Other - please specify

**Partnership at-a-glance (Four W's):** 
1. **Who** is the Partner?
1. **What** will the Partner be doing under this agreement/partnership?
1. **When** do does the agreement need to be closed (i.e., is it time sensitive) and what is driving the timeline?
1. **Why** is this relationship valuable to GitLab?

**Summary of Request**

Please provide a brief summary of your request and the negotiations to date here. In addition, use the chart below to detail the material terms of the relationship and any terms that require cross functional collaboration and/or approval.  

Cross Functional Approvers: _(tag any internal stakeholders who will need to provide input or approval here e.g. Sales Leadership / Finance / Product / Support)_

| Material Relationship Terms | Description |
| ------ | ------ |
| Net ARR | _What is the target ARR for this relationship?_
| Business Justification | _Detail business justification for the relationship (i.e., growth timelines, strategic justification...etc.)._ |
| Term | _What is the term of the Agreement and what are GitLab's termination rights?_ |
| Region/Segments | _Identify the region/segments this relationship will cover._ |
| Commercial Terms  | _Identify material commercial terms, including non-standard terms (i.e., discounts, SLAs, commitments, payment terms, etc...) e.g. $500K ARR Yr / $1M ARR Yr 2._ |
| Other  | _Please specify any other relevant material information you feel is important that is not already covered above._ |

_**Note:**_ Legal will assist in drafting a summary of any material non-standard legal and commercial terms that require cross functional input or approval in connection with this issue based upon the information provided by the Requestor.

**Do Not Edit Below (except for additional assistance tags)**

/confidential 
/assign @dcolesjr 
/label New ~"Legal Alliance Partner"
/additional assistance @m_taylor* (*additional assistance tags should only be included when @dcolesjr is out of the office or otherwise unavailable)